extends Node

class_name AnimationControl

onready var movement_input : MovementInput = get_parent().get_node("MovementInput")
onready var animation_player : AnimationPlayer = get_parent().get_node("AnimationPlayer")

func _ready():
	movement_input.connect("jump", self, "trigger_jump_animation")

func _process(delta):
	if movement_input.is_walking() and animation_player.current_animation != "jump":
		if animation_player.current_animation != "walking":
			animation_player.play("walking")
	if movement_input.is_idle():
		if animation_player.current_animation != "idle":
			animation_player.play("idle", 0.5)

func trigger_jump_animation():
	animation_player.play("jump")
