extends Sprite

export var duration : float = 1
export var timing_offset : float = 0
export var start_size : float = 1
export var end_size : float = 0

var timer : float = 0

func _process(delta):
	if timer > duration:
		timer -= duration
	var delta_size : float = end_size - start_size
	scale = Vector2.ONE * (start_size + delta_size * fmod(timer + timing_offset, duration) / duration)
	timer += delta
