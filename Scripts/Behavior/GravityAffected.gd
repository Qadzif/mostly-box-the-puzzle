extends Node

class_name GravityAffected

var movement_control : MovementControl
var gravity_modifier : Vector2 = Vector2.DOWN

func _ready():
	movement_control = get_parent().get_node("MovementControl")

func _physics_process(delta):
	movement_control.add_velocity(gravity.consts * delta, gravity_modifier)
