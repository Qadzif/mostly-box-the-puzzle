extends Node

class_name MovementControl

const SET_BOTH = 0b11
const SET_X = 0b10
const SET_Y = 0b01

var body : KinematicBody2D
var base_velocity : Vector2 = Vector2.ZERO
var added_velocity : Vector2 = Vector2.ZERO
var modified_velocity : Vector2 = Vector2.ZERO
var after_velocity : Vector2 = Vector2.ZERO
var stored_velocity : Vector2 = Vector2.ZERO
var velocity_modifier : Vector2 = Vector2.ONE
var velocity_vector : Vector2 = Vector2.ZERO

var freezed : bool = false

func _ready():
	body = get_parent()

func _physics_process(delta):
#	modified_velocity = body.move_and_slide(base_velocity * velocity_modifier, Vector2.UP)
#	base_velocity = modified_velocity / velocity_modifier
	if not freezed:
		base_velocity = body.move_and_slide(base_velocity, Vector2.UP)

func process_modifier():
	velocity_modifier = Vector2.ONE

func add_velocity(added_velocity : float, direction : Vector2 = Vector2.ONE):
	base_velocity += direction * added_velocity

func set_velocity(new_velocity : Vector2, component : int = SET_BOTH):
	if component & SET_X:
		base_velocity.x = new_velocity.x
	if component & SET_Y:
		base_velocity.y = new_velocity.y

func freeze():
	if not freezed:
		freezed = true
		stored_velocity = base_velocity

func unfreeze():
	if freezed:
		freezed = false
		base_velocity = stored_velocity
