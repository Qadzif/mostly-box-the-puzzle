extends CanvasLayer

var hidden : bool = true

func _ready():
	pass

func hide_canvas():
	get_tree().paused = false
	transform = Transform2D(Vector2.LEFT, Vector2.UP, Vector2.ZERO)
	hidden = true

func show_canvas():
	get_tree().paused = true
	transform = Transform2D(Vector2.RIGHT, Vector2.DOWN, Vector2.ZERO)
	hidden = false

func _input(event):
	if event.is_action_pressed("ui_cancel"):
		if hidden:
			global.pause_physics = true
			global.pause_timer()
			show_canvas()
		else:
			global.pause_physics = false
			global.activate_timer()
			hide_canvas()
