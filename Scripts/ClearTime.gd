extends Label

func _ready():
	global.pause_physics = true
	global.pause_timer()
	text = "Clear Time : {time}s".format({"time" : global.timer})
