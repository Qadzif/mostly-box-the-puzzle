extends Area2D

export var next : String

func _on_Body_Enter(body : Node):
#	print_debug("body enter")
	global.assign_main_player(body)
	get_tree().current_scene.remove_child(body)
	get_tree().change_scene("res://Scenes/Level/" + next + ".tscn")
