extends Node2D

var timer : float = 0

func _process(delta):
	var val = sin(timer)
	var val_sign = sign(val)
	rotation_degrees += val * val * 3 * val_sign
	timer += delta
