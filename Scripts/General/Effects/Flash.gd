extends TileMap

export var duration : float = 0.5

var timer : float = 0.0

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if timer > 0:
		$Mask.color.a = 1 - abs(timer / duration - 0.5) * 2
		timer -= delta
		if timer < 0:
			timer = 0
	pass

func start_animation():
	timer = duration

func _input(event):
	if event.is_action_pressed("ui_accept"):
		start_animation()
