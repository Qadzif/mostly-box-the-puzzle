extends Node

export var scene_path : String

func load_scene(path : String = ""):
	global.start_level = true
	if path == "":
		get_tree().change_scene("res://Scenes/" + scene_path + ".tscn")
	else:
		get_tree().change_scene("res://Scenes/" + path + ".tscn")
