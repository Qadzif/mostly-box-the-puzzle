extends Node

var pause_physics : bool = false
var gravity : float = 10
var main_player : Node
var timer : float = 0
var timer_count : bool = false
var start_pos : Vector2 = Vector2.ZERO
var start_vel : Vector2 = Vector2.ZERO
var start_level : bool = true
var show_hint : bool = false

func assign_main_player(player : Node):
	main_player = player

func is_player_on_floor():
	return main_player.is_on_floor()

func activate_timer(): 
	timer_count = true

func pause_timer():
	timer_count = false

func reset_timer():
	timer = 0

func restart_timer():
	reset_timer()
	activate_timer()

func _process(delta):
	if timer_count and not pause_physics:
		timer += delta
