extends Node2D

var next_map_name : String
var next_map_node : Node
var thread : Thread = Thread.new()
var loader : ResourceInteractiveLoader

func _ready():
#	print_debug(get_children())
	pass

func load_next_map_thread():
	print("loading next map thread")
	call_deferred("print_def")
	next_map_node = load(next_map_name).instance()
	print_debug(next_map_node)

func print_def():
	print_debug("def")

func _process(delta):
	if loader == null:
		set_process(false)
		return
	var err = loader.poll()
	if err == ERR_FILE_EOF:
		print_debug("finish load map")
		next_map_node = loader.get_resource().instance()
		loader = null
	elif err == OK:
		pass
	else:
		pass

func load_next_map(next_map : String):
	print_debug("loading next map")
	loader = ResourceLoader.load_interactive("res://Scenes/Level/" + next_map + ".tscn")
	set_process(true)

func to_next_map():
	empty_child()
	add_child(next_map_node)

func empty_child():
	for child in get_children():
		remove_child(child)
