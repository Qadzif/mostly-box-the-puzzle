extends TileMap

var thin_platform_shapes : Array

func _ready():
	global.pause_physics = false
	global.activate_timer()
	thin_platform_shapes = tile_set.tile_get_shapes(2)
	if not global.start_level:
		add_child(global.main_player)
	else:
		global.timer = 0
		$PlayerSpawner.spawn_player()
	global.start_level = false

func _input(event):
	if event.is_action_pressed("ui_down"):
		tile_set.tile_set_shapes(2, [])
	if event.is_action_released("ui_down"):
		tile_set.tile_set_shapes(2, thin_platform_shapes)
	if event.is_action_pressed("hint"):
		$Hint.visible = true
	if event.is_action_released("hint"):
		$Hint.visible = false

func _exit_tree():
	tile_set.tile_set_shapes(2, thin_platform_shapes)
