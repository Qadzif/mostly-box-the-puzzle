extends Node

class_name MovementInput

const MOVING_FREEZE = -1
const MOVING_STOP = 0
const MOVING_RIGHT = 1
const MOVING_LEFT = 2

signal jump

var body : KinematicBody2D
var movement_control : MovementControl
export var movement_speed : float = 450
export var jump_speed : float = 450
var moving_right : bool = false
var moving_left : bool = false
var current_moving : int = MOVING_STOP
var last_moving : int = MOVING_STOP
var jumping : bool = false

func _ready():
	body = get_parent()
	movement_control = get_parent().get_node("MovementControl")

func _input(event):
	if current_moving != MOVING_FREEZE:
		if event.is_action_pressed("move_right"):
			moving_right = true
			current_moving = MOVING_RIGHT
		if event.is_action_pressed("move_left"):
			moving_left = true
			current_moving = MOVING_LEFT
		if event.is_action_released("move_right"):
			moving_right = false
			if moving_left:
				current_moving = MOVING_LEFT
			else:
				stop_move()
		if event.is_action_released("move_left"):
			moving_left = false
			if moving_right:
				current_moving = MOVING_RIGHT
			else:
				stop_move()
		if event.is_action_pressed("move_up"):
			jumping = true
		if event.is_action_released("move_up"):
			jumping = false
	if event.is_action_pressed("seek"):
		freeze_movement()
	if event.is_action_released("seek"):
		unfreeze_movemnet()

func _process(delta):
	if jumping and body.is_on_floor():
		jump()
	match current_moving:
		MOVING_RIGHT:
			move_right()
		MOVING_LEFT:
			move_left()

func is_walking():
	return body.is_on_floor() and (current_moving == MOVING_RIGHT or current_moving == MOVING_LEFT)

func is_idle():
	return body.is_on_floor() and current_moving == MOVING_STOP

func is_on_air():
	return not body.is_on_floor()

func move_right():
	movement_control.set_velocity(Vector2.RIGHT * movement_speed, MovementControl.SET_X)

func move_left():
	movement_control.set_velocity(Vector2.LEFT * movement_speed, MovementControl.SET_X)

func jump():
	movement_control.set_velocity(Vector2.UP * jump_speed, MovementControl.SET_Y)
	emit_signal("jump")

func stop_move():
	current_moving = MOVING_STOP
	movement_control.set_velocity(Vector2.ZERO, MovementControl.SET_X)

func freeze_movement():
	last_moving = current_moving
	current_moving = MOVING_FREEZE
	movement_control.freeze()

func unfreeze_movemnet():
	current_moving = last_moving
	movement_control.unfreeze()
