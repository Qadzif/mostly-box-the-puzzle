extends KinematicBody2D

class_name PlayerBody

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

export var gravity : float = 10
export var jump_power : float = 10
export var movement_speed : float = 10

var velocity : Vector2 = Vector2.ZERO
var stop_horizontal : bool = false
var moving_right : bool = false
var moving_left : bool = false

# Called when the node enters the scene tree for the first time.
func _ready():
	global.assign_main_player(self)
	pass # Replace with function body.

func _process(delta):
	if not global.pause_physics:
		my_process(delta)

func my_process(delta):
	velocity += Vector2.DOWN * gravity * delta
	if moving_right and not moving_left:
		velocity.x = movement_speed
	if moving_left and not moving_right:
		velocity.x = -movement_speed
	if is_on_floor() and not moving_left and not moving_right:
		velocity.x = 0
	velocity = move_and_slide(velocity, Vector2.UP)

func _input(event):
	if is_on_floor() and event.is_action("ui_up"):
		velocity += Vector2.UP * jump_power
	if event.is_action_pressed("ui_left"):
		velocity.x = -movement_speed
		moving_left = true
	if event.is_action_pressed("ui_right"):
		velocity.x = movement_speed
		moving_right = true
	if event.is_action_released("ui_left"):
		moving_left = false
	if event.is_action_released("ui_right"):
		moving_right = false
	if event.is_action_pressed("seek"):
		global.pause_physics = true
	if event.is_action_released("seek"):
		global.pause_physics = false
	pass
