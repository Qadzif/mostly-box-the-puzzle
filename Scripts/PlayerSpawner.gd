extends Node2D

class_name PlayerSpawner

export var player_resource : Resource

func spawn_player():
	var spawned_player : Node2D = player_resource.instance()
	spawned_player.position = position
	get_parent().add_child(spawned_player)
