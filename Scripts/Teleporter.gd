extends Area2D

signal player_teleport

var is_player_inside : bool = false
var player : Node2D

func _on_Player_Enter(body : Node2D):
	if body.name == "PlayerBody":
		player = body
		is_player_inside = true
	pass


func _on_Player_Exit(body : Node2D):
	if body.name == "PlayerBody":
		is_player_inside = false
	pass # Replace with function body.

func _input(event):
	if is_player_inside and event.is_action_pressed("tele"):
		emit_signal("player_teleport")
		$PopOut.position = $Target.position
		$PopOutPlayer.play("popout")
		player.position += $Target.position
